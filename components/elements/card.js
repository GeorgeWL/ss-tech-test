import React from "react";

function Header({ children, className = "" }) {
  return (
    <div
      className={`bg-cool-gray-50 text-gray-700 text-lg rounded-t p-4 ${className}`}
    >
      {children}
    </div>
  );
}
function Body({ children, className = "" }) {
  return <div className={`rounded-b p-4 ${className}`}>{children}</div>;
}

export function Card({ children, className = "" }) {
  return (
    <div className={`bg-white rounded shadow ${className}`}>{children}</div>
  );
}

Card.Header = Header;
Card.Body = Body;
