import React from "react";
import PropTypes from "prop-types";
import c from "classnames";

const Button = React.forwardRef(
  (
    {
      color = "blue",
      size = "md",
      children,
      className = "",
      loading = false,
      spanClass,
      ...props
    },
    ref
  ) => {
    const colorClass = c({
      "inline-flex items-center border border-transparent font-medium rounded focus:outline-none transition ease-in-out duration-150 whitespace-no-wrap": true,
      "text-white bg-blue-500 hover:bg-blue-600 focus:border-blue-500 focus:shadow-outline-blue active:bg-blue-600":
        color === "blue",
      "text-white bg-green-400 hover:bg-green-500 focus:border-green-500 focus:shadow-outline-green active:bg-green-500":
        color === "green",
      "text-white bg-red-400 hover:bg-red-500 focus:border-red-500 focus:shadow-outline-red active:bg-red-500":
        color === "red",
      "text-white bg-indigo-500 hover:bg-indigo-600 focus:border-indigo-600 focus:shadow-outline-indigo active:bg-indigo-600":
        color === "indigo",
      "border-gray-300 text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50":
        color === "white",
      "text-white bg-orange-400 hover:bg-orange-500 focus:border-orange-400 focus:shadow-outline-orange active:bg-orange-500":
        color === "orange",
      "text-gray-700 hover:text-white bg-gray-200 hover:bg-gray-400 focus:bg-gray-400 focus:text-white focus:border-gray-200 focus:shadow-outline-gray active:bg-gray-500":
        color === "gray",
      "text-gray-600 hover:text-gray-700 focus:text-gray-800 hover:bg-cool-gray-100 focus:shadow-outline-gray active:text-gray-500":
        color === "link",
    });

    const sizeClass = c({
      "text-xs leading-4 px-2 py-1": size === "xs",
      "text-sm leading-4 px-3 py-2": size === "sm",
      "text-sm leading-5 px-4 py-2": size === "md",
      "text-base leading-6 px-4 py-2": size === "lg",
    });

    return (
      <span
        className={`${
          color !== "link"
            ? `relative inline-flex rounded-md shadow-sm ${spanClass}`
            : ""
        }`}
      >
        <button
          ref={ref}
          type="button"
          className={`${colorClass} ${sizeClass} ${className}`}
          {...props}
        >
          {children}
        </button>
      </span>
    );
  }
);

Button.propTypes = {
  color: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.any,
};

Button.defaultProps = {
  type: "button",
  color: "blue",
  size: "md",
  icon: false,
  onClick: () => {},
};

export default Button;
