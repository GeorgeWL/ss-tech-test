import React from "react";
import { Card, Button } from "components/elements";

const Form = () => {
  return (
    <Card className="">
      <Card.Header>Parents Evening title</Card.Header>
      <Card.Body>
        <div className="grid gap-8 p-8">
          <div className="space-y-2">
            <label className="label">Title</label>
            <input className="form-input w-full" placeholder="Title" />
          </div>
          <div className="space-x-2 text-right">
            <Button color="link">Cancel</Button>
            <Button color="white">Save as Draft</Button>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
};

export default Form;
